<?php
namespace vendor\pillax\cli\src;

class Cli {

    /**
     * Detect we are in cli environment
     *
     * @return bool
     */
    public static function isCli() : bool {
        return php_sapi_name() == 'cli';
    }

    /**
     * Route console call to controller action
     *
     * Example:
     *
     * shell:
     * #php index.php test testMe 'First method parameter'
     *
     * This will call
     * '\app\controllers\testController()->testMe('First method parameter');
     *
     * @param $arguments
     * @throws CliException
     */
    public static function callController(?array $arguments=[]) : void {
        $controller = '\app\controllers\\' . $arguments[1] . 'Controller';
        $action     = $arguments[2];

        if( ! is_callable([$controller, $action])) {
            throw new CliException(CliException::NOT_FOUND, [
                'controller'   => $controller,
                'action'       => $action,
            ]);
        }

        // Remove everything except arguments
        unset(
            $arguments[0], // index.php
            $arguments[1], // controller name
            $arguments[2]  // method name
        );

        call_user_func_array([new $controller, $action], $arguments);
    }


}