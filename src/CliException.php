<?php
namespace vendor\pillax\cli\src;

use vendor\pillax\simpleException\src\ExceptionInterface;

class CliException extends \Exception implements ExceptionInterface {
    const NOT_FOUND = 1;

    private $messages = [
        self::NOT_FOUND => 'Requested url not found',
    ];

    private $details = [];

    public function __construct($code, array $details = []) {
        $this->details = $details;
        parent::__construct($this->messages[$code], $code, null);
    }

    public function getDetails() {
        return $this->details;
    }
}